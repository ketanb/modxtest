<?php
/**
 * @file
 * Contains \Drupal\hello_world\Controller\HelloController.
 */
namespace Drupal\modxtest\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;


class ModxtestController {
    public function content() {
		
		//! Fetch the data
		$content = file_get_contents("https://staging-beta.qsleap.com/modx/webinar");
        
        //! Decode the data
        $webinars = json_decode($content);
        
        $sitename = "";
        
        //! For Drupal setup 
        if(class_exists('\Drupal')) {
			$config = \Drupal::config('system.site');
			$sitename = strtolower($config->get('name'));
		} else {
			$sitename = "topuniversities";
		}
        
        //! Pass the data to the view 
        return array(
			'#theme' => 'modxtest_theme',
			'#webinars' => $webinars,
			'#sitename' => $sitename,
			'#title' => "Webinars",
			'#user' => null
		);
    }
    
    public function webinar($slug = '') {
		
		//! Fetch the data
		$content = file_get_contents("https://staging-beta.qsleap.com/modx/webinarlandingpage/" . $slug);
        
        //! Decode the data 
        $content_obj = json_decode($content);
        
        //! Check for valid slug 
        if(@$content_obj->error == "404"){
			$routeName = 'modxtest.content';
			$url = \Drupal::url($routeName);
			return new RedirectResponse($url);
			
		}
		
		$sitename = "";
        
        //! For Drupal setup 
        if(class_exists('\Drupal')) {
			$config = \Drupal::config('system.site');
			$sitename = strtolower($config->get('name'));
		} else {
			$sitename = "TopUniversities";
		}
        
        //! Pass the data to the view 
        return array(
			'#theme' => 'webinar_single_page',
			'#webinar' => $content_obj->webinar,
			'#webinars' => $content_obj->webinars,
			'#registrations' => $content_obj->registrations,
			'#sitename' => $sitename,
			'#title' => $content_obj->webinar->title,
			'#user' => null
		);
	}
}
?>
